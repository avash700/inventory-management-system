'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class sales extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Items, SalesItems}) {
      // define association here
      this.belongsToMany(Items, {
        through: SalesItems,
        foreignKey: 'sale_id'
      })
    }
  }
  sales.init({
    sale_to: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    sale_address: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    sale_contact: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bill_number: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sale_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    is_complete: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Sales',
    tableName: 'sales'
  });
  return sales;
};