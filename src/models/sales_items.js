'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class sales_items extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Sales, Items}) {
      // define association here
      this.belongsTo(Items, {
        foreignKey: 'item_id'
      }) 

      this.belongsTo(Sales, {
        foreignKey: 'sale_id'
      })
    }
  }
  sales_items.init({
    sale_id: {
      allowNull: true,
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'sales'
        },
        key: 'id'
      }
    },
    item_id: {
      allowNull: true,
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'items'
        },
        key: 'id'
      }
    },
    sale_quantity: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
    price: {
      type: DataTypes.FLOAT.UNSIGNED,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'SalesItems',
    tableName: 'sales_items'
  });
  return sales_items;
};