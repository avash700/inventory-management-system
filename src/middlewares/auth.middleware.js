const { validateToken } = require("../utils/jwt")

const checkAuth = (userRole = []) => {
    if (typeof (userRole) === "string") {
        userRole = [userRole]
    }

    // return another middleware
    return async (req, res, next) => {
        if (req.headers.authorization) {
            const token = req.headers.authorization.split(" ")[1];

            const decoded = await validateToken(token);
            if (decoded.error) {
                return res.status(401).send({ error: decoded.error });
            }
            
            if (!userRole.length) {
                return res.status(401).json({
                    error: 'You are not authorized.'
                });
            }

            if(!userRole.includes(decoded.data.role_type)){
                return res.status(401).json({
                    error: 'You are not authorized.'
                });
            }

            return next();
        } else {
            return res.status(401).send({ error: "no token provided!" });
        }
    }
}

module.exports = {
    checkAuth
}