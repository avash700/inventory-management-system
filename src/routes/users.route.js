const router = require('express').Router();
const userController = require('../controllers/users.controller');

/**
    * @openapi
    * /user:
    *   get:
    *     tags: [user]
    *     summary: returns all users
    *     responses:
    *        '200':    
    *            description: A JSON with array of available users
    *        '500':
    *            description: internal server error
    *   post:
    *     tags: [user]
    *     summary: add a new user
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          role_id:
    *                              type: integer
    *                          name:
    *                              type: string
    *                          emal:
    *                              type: string
    *                          password:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: user created
    *         '404':
    *             description: user already exists
    *         '500':
    *             description: internal server error
    * 
    * 
    * /user/{userEmail}:
    *   delete:
    *       tags: [user]
    *       summary: delete a user
    *       parameters:
    *         - name: userEmail
    *           in: path
    *           required: true
    *           description: the id of the user to delete 
    *       responses:
    *           '200':    
    *               description: user deleted
    *           '404':
    *               description: user doesn't exist
    *           '500':
    *               description: internal server error
    * 
    *   put:
    *       tags: [user]
    *       summary: update a user
    *       parameters:
    *         - name: userEmail
    *           in: path
    *           required: true
    *           description: the id of the user to update
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            role_id:
    *                                type: integer
    *                            name:
    *                                type: string
    *                            emal:
    *                                type: string
    *                            password:
    *                                type: string
    *       responses:
    *           '200':    
    *               description: user updated
    *           '404':
    *               description: user doesn't exist
    *           '409':
    *               description: user already exists
    *           '500':
    *               description: internal server error
    *          
    */


router.route('/')
    .get(userController.getUsers)
    .post(userController.createUsers)
    
router.route('/:email')
    .delete(userController.deleteUsers)
    .put(userController.updateUsers)

module.exports = router



