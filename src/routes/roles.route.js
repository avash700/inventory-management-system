const router = require('express').Router();
const roleController = require('../controllers/roles.controller');

/**
    * @openapi
    * /roles:
    *   get:
    *     tags: [roles]
    *     summary: returns all roles
    *     responses:
    *        '200':    
    *            description: A JSON with array of available roles
    *        '401':
    *            description: Unauthorized               
    *        '500':
    *            description: internal server error
    *   post:
    *       tags: [role]
    *       summary: add a new role
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *    
    *                            role_type:
    *                                type: string
    *                         
    *       responses:
    *           '200':    
    *               description: role created
    *           '404':
    *               description: role already exists
    *           '500':
    *               description: internal server error
    * 
    * 
    * /roles/{roleId}:
    *    delete:
    *       tags: [role]
    *       summary: delete a role
    *       parameters:
    *         - name: roleId
    *           in: path
    *           required: true
    *           description: the id of the role to delete 
    *       responses:
    *           '200':    
    *               description: role deleted
    *           '404':
    *               description: role doesn't exist
    *           '500':
    *               description: internal server error
    * 
    *    put:
    *       tags: [role]
    *       summary: update a role
    *       parameters:
    *         - name: roleId
    *           in: path
    *           required: true
    *           description: the id of the role to update
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            role_type:
    *                                type: string
    *                            
    *       responses:
    *           '200':    
    *               description: role updated
    *           '404':
    *               description: role doesn't exist
    *           '409':
    *               description: role already exists
    *           '500':
    *               description: internal server error
    *          
    */

router.route('/')
    .get(roleController.getRoles)
    .post(roleController.createRoles)
    

router.route('/:id')
    .delete(roleController.deleteRoles)
    .put(roleController.updateRoles)

module.exports = router

