const router = require('express').Router();
// import and use all routes here as required
const categoriesRoute = require('./categories.route')
const brandsRoute = require('./brands.route')
const itemsRoute = require('./items.route')
const vendorsRoute = require('./vendors.route')
const orderStatusRoute = require('./orderstatuses.route')
const orderLinesRoute = require('./orderlines.route')
const inventoryRoute = require('./inventory.route')
const userRoutes = require('./users.route')
const roleRoutes = require('./roles.route')
const salesRoutes = require('./sales.route')
const authRoute =  require('./auth.route')
const { checkAuth } = require("../middlewares/auth.middleware");


router.use('/auth', authRoute)
router.use('/categories', checkAuth("admin") , categoriesRoute)
router.use('/brands', checkAuth("admin") ,brandsRoute)
router.use('/items',checkAuth("admin"),itemsRoute)
router.use('/order-lines', checkAuth(["admin","employee"]),orderLinesRoute)
router.use('/order-status', checkAuth("admin"),orderStatusRoute)
router.use('/vendors', checkAuth("admin"),vendorsRoute)
router.use('/user',checkAuth("admin"),userRoutes)
router.use('/role',checkAuth("admin"),roleRoutes)
router.use('/inventory', checkAuth("admin"), inventoryRoute)
router.use('/sale',checkAuth(["admin","employee"]),salesRoutes)

module.exports = router;


