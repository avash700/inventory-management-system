const router = require('express').Router();
const authController = require('../controllers/auth.controller');

/**
    * @openapi
    * /auth/login:
    *   post:
    *     tags: [auth]
    *     summary: login a user
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          email:
    *                              type: string
    *                          password:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: user logged in
    *         '404':
    *             description: user does not exist
    *         '401':
    *             description: invalid credentials
    *         '500':
    *             description: internal server error
    * /auth/change-password:
    *   post:
    *     tags: [auth]
    *     summary: login a user
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          old_password:
    *                              type: string
    *                          new_password:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: password changed
    *         '404':
    *             description: user does not exist
    *         '401':
    *             description: invalid credentials
    *         '500':
    *             description: internal server error
    * 
    * /auth/forgot-password:
    *   post:
    *     tags: [auth]
    *     summary: login a user
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          email:
    *                              type: string
    *                          password:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: success
    *         '404':
    *             description: user does not exist
    *         '500':
    *             description: internal server error
    * /auth/reset-password:
    *   post:
    *     tags: [auth]
    *     summary: login a user
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          email:
    *                              type: string
    *                          password:
    *                              type: string
    *                          otp:
    *                              type: integer
    *     responses:
    *         '200':    
    *             description: password reset
    *         '404':
    *             description: user does not exist
    *         '401':
    *             description: invalid credentials
    *         '500':
    *             description: internal server error
    */

router.route('/login')
    .post(authController.loginUser)

module.exports = router



