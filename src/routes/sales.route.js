const router = require('express').Router();
const salesController = require('../controllers/sales.controller');
const { validator, saleRules, updateSaleRules, addItemSalesRules, updateSaleItemRules } = require('../middlewares/validator.middleware')

/**
    * @openapi
    * /sale:
    *   get:
    *     tags: [sale]
    *     summary: returns all sales
    *     responses:
    *        '200':    
    *            description: A JSON with array of available sales
    *        '500':
    *            description: internal server error
    *   post:
    *       tags: [sale]
    *       summary: add a new sales
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            sale_to:
    *                                type: string
    *                            sale_address:
    *                                type: string
    *                            sale_contact:
    *                                type: string
    *                            description:
    *                                type: string
    *                            bill_number:
    *                                type: integer
    *                            sale_date:
    *                                type: string
    *                                format: date
    *                            items:
    *                                type: array
    *                                items:
    *                                   type: object
    *                                   properties:
    *                                       item_id:
    *                                           type: integer
    *                                       price:
    *                                           type: integer
    *                                       order_quantity:
    *                                           type: integer
    * 
    *       responses:
    *           '201':    
    *               description: sale created
    *           '404':
    *               description: requested item doesnt exist
    *           '409':
    *               description: duplicate bill number
    *           '500':
    *               description: internal server error
    * 
    * 
    * /sale/{saleId}:
    *    delete:
    *       tags: [sale]
    *       summary: delete an sale
    *       parameters:
    *         - name: saleId
    *           in: path
    *           required: true
    *           description: the id of the sale to delete 
    *       responses:
    *           '200':    
    *               description: sale deleted
    *           '404':
    *               description: saledoesn't exist
    *           '500':
    *               description: internal server error
    * 
    *    patch:
    *       tags: [sale]
    *       summary: update an sale
    *       parameters:
    *         - name: orderLineId
    *           in: path
    *           required: true
    *           description: the id of the sale to update
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            sale_to:
    *                                type: string
    *                            sale_address:
    *                                type: string
    *                            sale_contact:
    *                                type: string
    *                            description:
    *                                type: string
    *                            bill_number:
    *                                type: integer
    *                            sale_date:
    *                                type: string
    *                                format: date
    *       responses:
    *           '200':    
    *               description: sale updated
    *           '404':
    *               description: sale doesn't exists
    *           '409':
    *               description: duplicate bill number
    *           '500':
    *               description: internal server error
    * 
    * /sale/{saleId}/item/{itemId}:
    *    post:
    *       tags: [sale]
    *       summary: add item to an sale
    *       parameters:
    *         - name: saleId
    *           in: path
    *           required: true
    *           description: the id of the sale to add item
    *         - name: itemId
    *           in: path
    *           required: true
    *           description: the id of the item to be added in the sale
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            price:
    *                                type: integer
    *                            sale_quantity:
    *                                type: integer 
    *
    *       responses:
    *           '201':    
    *               description: item added to the sale
    *           '400':
    *               description: sale is already complete
    *           '404':
    *               description: sale doesn't exist or requested item doesn't exist
    *           '409':
    *               description: item already exists in the sale
    *           '500':
    *               description: internal server error
    * 
    *    delete:
    *       tags: [sale]
    *       summary: delete item from an sale
    *       parameters:
    *         - name: saleId
    *           in: path
    *           required: true
    *           description: the id of the sale to delete item
    *         - name: itemId
    *           in: path
    *           required: true
    *           description: the id of the item to be deleted from the sale
    *
    *       responses:
    *           '200':    
    *               description: item deleted from the sale
    *           '404':
    *               description: sale doesn't exist
    *           '500':
    *               description: internal server error
    * 
    *    patch:
    *       tags: [sale]
    *       summary: update item in an sale
    *       parameters:
    *         - name: saleId
    *           in: path
    *           required: true
    *           description: the id of the sale to update item
    *         - name: itemId
    *           in: path
    *           required: true
    *           description: the id of the item to be updated in the sale
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            price:
    *                                type: integer
    *                            sale_quantity:
    *                                type: integer 
    *
    *       responses:
    *           '200':    
    *               description: item updated in the sale
    *           '400':
    *               description: sale is already complete
    *           '404':
    *               description: sale doesn't exist or requested item doesn't exist
    *           '500':
    *               description: internal server error
    * 
    
    * /sale/complete-sale/{saleId}:
    *    post:
    *       tags: [sale]
    *       summary: update status of sale to completed
    *       parameters:
    *         - name: saleId
    *           in: path
    *           required: true
    *           description: the id of the sale to complete
    *
    *       responses:
    *           '200':    
    *               description: sale completed
    *           '400':
    *               description: sale is already complete
    *           '404':
    *               description: sale doesn't exist or requested item doesn't exist
    *           '500':
    *               description: internal server error
    * 
    * /sale/get-sale-pdf/{saleId}:
    *    get:
    *       tags: [sale]
    *       summary: get purchase order pdf of the order line
    *       parameters:
    *         - name: saleId
    *           in: path
    *           required: true
    *           description: the id of the sale to get sale pdf
    *
    *       responses:
    *           '200':    
    *               description: a pdf file
    *           
    *           '404':
    *               description: sale doesn't exist or requested item doesn't exist
    *           '500':
    *               description: internal server error
    * 
    */

router.route('/')
    .get(salesController.index)
    .post(saleRules() ,validator ,salesController.create)
        
router.route('/complete-sale/:id')
    .post(salesController.completeSale)
    

router.route('/:id')
    .delete(salesController.destroy)
    .patch(updateSaleRules(), validator ,salesController.update)

router.route('/:id/item/:itemId')
    .post(addItemSalesRules(), validator ,salesController.addItem)
    .delete(salesController.removeItem)
    .patch(updateSaleItemRules(), validator ,salesController.updateItem)

router.route('/get-sale-pdf/:id')
    .get(salesController.getSalePdf)

module.exports = router

