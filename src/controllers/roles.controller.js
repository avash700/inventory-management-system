const {Role} = require('../models');
const _ = require('lodash');

// get roles
const getRoles = async (req, res) => {
    try {
        const role = await Role.findAll({
            attributes: ["id","role_type"],
        });
        return res.status(200).send(role);

    } catch(e) {
        res.status(500).json({ error: e })
    }
}

// create a new role
const createRoles = async (req, res) => {
    try {
        const body = _.pick(req.body, ['role_type']);

        const oldRole = await Role.findOne({
            where: {
                role_type: body.role_type
            }
        })

        if(oldRole) {
            return res.status(409).send({
                "message": "Role already exists."
            })
        } else {
            await Role.create(body);
            return res.status(201).send({
                "message": "Role created successfully."
            });s
        }
    } catch(e) {
        res.status(500).json({ error: e })
    }
}

// update role
const updateRoles = async (req, res) => {
    try {
        const {id} = req.params;

        const oldRole = await Role.findOne({
            where: {
                id: id
            }
        })

        if(oldRole) {
            await Role.update({
                role_type: body.role_type
            }, {
                where: {
                    id:id
                }
            });
            return res.status(200).send({
                "message": "Update Successfull."
            });
        } 
        else {
            return res.status(404).send({
                "message": "Role doesn't exists."
            });
        }
    } catch (e) {
        res.status(500).json({ error: e })
    }
}

// delete an existing roles
const deleteRoles = async (req, res) => {
    try {
        const {id} = req.params;

         const oldUser = await Role.findOne({
             where: {
                id: id
            }
        });

        if(oldUser) {
            await Role.destroy({
            where: {
                id: id
            }
            });
            return res.status(200).send({
                "message": "Role deleted successfully."
            });
        }
        else {
            return res.status(404).send({
                "message": "Role not found."
            });
        }

    } catch (e) {
        res.status(500).json({ error: e })
    }
}


module.exports = {
    getRoles,
    createRoles,
    updateRoles,
    deleteRoles
}