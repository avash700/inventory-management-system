const {User, Role} = require('../models')
const {hashPassword} = require('../utils/bcrypt');
const _ = require('lodash');

// get users
const getUsers = async (req, res) => {
    try {
        const user = await User.findAll({
            attributes: ["id","firstName","lastName","email"],
            raw: true,
            include: [
                {
                    model: Role,
                    attributes: ["id","role_type"],
                }
            ]
        });
        return res.status(200).send(user);
    } catch (e) {
        res.status(500).json({ error: e })
    }
}

// create a new user
const createUsers = async (req, res) => {
    try {
        const body = _.pick(req.body, ['firstName', 'lastName', 'email', 'password', 'role_id']);
        const oldUser = await User.findOne({
            where: {
                email: body.email
            }
        })

        if(!oldUser){
            body.password = hashPassword(body.password);
            await User.create(body);
            return res.status(201).send({
                "message": "User created successfully."
            })
        }
        else {
            return res.status(409).send({
                "message": "User already exists"
            })
        }
    } catch (e) {
        res.status(500).json({ error: e })
    }
}

// update a user
const updateUsers = async (req, res) => {
    try {
        const body = _.pick(req.body, ['firstName', 'lastName', 'email', 'password', 'role_id']);
        const {email} = req.params;

        const oldUser = await User.findOne({
            where: {
                email: email
            }
        })
        if(oldUser) {
            await User.update({
                role_id: body.role_id
            }, {
                where: {
                    email: email
                }
            });
            return res.status(200).send({
                "message": "Update Successfully."
            });
        } 
        else {
            return res.status(404).send({
                "message": "User doesn't exists."
            });
        }
    } catch (e) {
        res.status(500).json({ error: e })
    }
}

// delete an existing user
const deleteUsers = async (req, res) => {
    try {
        const {email} = req.params;

         const oldUser = await User.findOne({
             where: {
            email: email
            }
        });

        if(oldUser) {
            await User.destroy({
            where: {
                email: email
            }
            });
            return res.status(200).send({
                "message": "User deleted successfully."
            });
        }
        else {
            return res.status(404).send({
                "message": "User not found."
            });
        }

    } catch (e) {
        res.status(500).json({ error: e })
    }
}

module.exports = {
    getUsers,
    createUsers,
    updateUsers,
    deleteUsers
}