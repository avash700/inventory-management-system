const { User, Role } = require("../models")
const { hashPassword, comparePassword } = require("../utils/bcrypt")
const { generateToken } = require("../utils/jwt")
const _ = require('lodash')

const loginUser = async (req, res, next) => {
    try {
        const body = _.pick(req.body, ['email', 'password']);
     
        const oldUser = await User.findOne({
            include: [{
                model: Role,
                attributes: ["role_type"],
            }],
            where: {
                email: body.email
            }
        });
        if (!oldUser) {
            return res.status(404).send({ error: "User not found" });
        }

        if (!comparePassword(body.password, oldUser.password)) {
            return res.status(401).send({ error: "Invalid credentials" });
        }

        const payload = { id: oldUser.id, email: oldUser.email, role_type: oldUser.Role.role_type}
        const token = generateToken(payload);

        return res.status(200).send({ message: "login success", token })
    } catch (e) {
        console.log(e)
        return res.status(500).json({ error: "internal server error" })
    }
}

module.exports = {
    loginUser
}