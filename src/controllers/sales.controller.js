const PDFDocument = require('pdfkit')
const { Items, Sales, SalesItems, Brands, Categories, Inventory } = require("../models")
const { pick } = require("../utils/index")

const index = async (_, res) => {
    try {
        const dbRes = await Sales.findAll({
            
            include: [{
                model: Items,
                attributes: ["id", "name", "description", "image_url", "sku_code"],
                include: [
                    {
                        model: Brands,
                        attributes: ["brand_name"]
                    },
                    {
                        model: Categories,
                        attributes: ["category_type"]
                    }
                ]
            }],
        })
        return res.status(200).json({ message: "success", data: dbRes, dataCount: dbRes.length })
    } catch (e) {
        res.status(500).json({ error: e })
    }
}

const create = async(req, res) => {
    try {
        const reqObj = pick(req.body, ["sale_to", "sale_address", "sale_contact", "items", "description", "bill_number", "sale_date"])
        const bill_number = await Sales.findOne({ 
            where: { 
                bill_number: reqObj.bill_number 
            } 
        })
        
        if (bill_number) {
            return res.status(409).json({ error: "The sale with the provided bill number already exists!" })
        }
        const itemErrors = await Promise.all(reqObj.items.map(async item => {
            const dbItem = await Inventory.findOne({
                where: {
                   item_id:item.item_id
                }
            });
            if (!dbItem) {
                return { error: `Requested item with id: ${item.item_id} doesn't exits!` }
            }
     
            if(item.sale_quantity > dbItem.in_stock) {
                return { error: `Requested item with id: ${item.item_id} is not in stock.` }
            }
        }))
        const newItemErrors = itemErrors.filter(el => {
            return el !== undefined;
        });
    
        if(newItemErrors.length > 0){
            return res.status(404).json({ errors: newItemErrors })   
        }
        // const stockErrors = await Promise.all(reqObj.items.map(async item => {
        //     const in_stock = await Inventory.findByPk(item.item_id)
        //     if(item.sale_quantity > in_stock.in_stock) {
        //         return res.status(409).json({ error: `Requested item with id: ${item.item_id} is not in stock.` })
        //     }
        // }))

        // const newStockErrors = stockErrors.filter(el => {
        //     return el !== undefined;
        // });
        // if(newStockErrors.length > 0){
        //     return res.status(404).json({ errors: newStockErrors })   
        // }
        
        const currentSales = await Sales.create(reqObj)
        
        const itemData = reqObj.items.map(item => ({
            ...item,
            sale_id: currentSales.id
        }))
        
        await SalesItems.bulkCreate(itemData,{
            fields: ["sale_id","item_id","price","sale_quantity"]
        });
        
        return res.status(201).json({ message: "successfully created a new sale line", data: currentSales });
    } catch (e) {
        res.status(500).json({ error: e })
    }
}

const destroy = async(req, res) => {
    try {
        const { id } = req.params
        const sales =  await Sales.findByPk(id)
        if(!sales) {
            return res.status(404).json({ error: 'Item not found!' })
        }
        const items = await sales.getItems()
        await sales.removeItems(items)
        await Sales.destroy({
            where: { id }
        })
        return res.status(200).json({ message: 'Sales deleted successfully!' })
    } catch (e) {
        res.status(500).json({ error: e })
    }
}

const update = async(req, res) => {
    try {
        const { id } = req.params
        // find if the category exits
        const sales = await Sales.findByPk(id)
        if(!sales) {
            return res.status(409).json({ error: "The item doesn't exist" })
        }
        const reqObj = removeUndefinedKeys(pick(req.body, ["sale_to", "sale_address","sale_contact",,"bill_number", "description", "sale_date"]));
        if (reqObj.bill_number) {
            const bill_number = await Sales.findOne({ 
                where: { 
                    bill_number: reqObj.bill_number 
                } 
            })
            if (bill_number) {
                return res.status(409).json({ error: "The sale with the provided bill number already exists!" })
            }
        }
        await Sales.update(reqObj, {
            where: { id }
        })
        return res.status(200).json({ message: 'Sale updated successfully!', data: reqObj })
    } catch (e) {
        res.status(500).json({ error: e })
    }
}


const completeSale = async(req, res) => {
    try {
        const { id } = req.params
        const sales = await Sales.findByPk(id, {
            include: [{
                model: Items,
                attributes: ["id", "name", "description", "image_url", "sku_code"],
                include: [
                    {
                        model: Brands,
                        attributes: ["brand_name"]
                    },
                    {
                        model: Categories,
                        attributes: ["category_type"]
                    }
                ]
            }],
        })
        if(!sales) {
            return res.status(409).json({ error: "The sale doesn't exist" })
        }
        if (sales.is_complete) {
            return res.status(409).json({ error: "The sale is already completed!" })
        }

        sales.Items.forEach( async item => {
            const inventoryItem = await Inventory.findOne({
                where: {
                    item_id: item.id
                }
            })
            const newStock = inventoryItem.in_stock - item.SalesItems.sale_quantity;
            await Inventory.update({ 
                in_stock: newStock 
            }, { 
                where: 
                { 
                    item_id: item.id 
                } 
            })
        })
        await Sales.update({ 
            is_complete: true 
        }, { 
            where: { 
                id
            } 
        });
        return res.status(200).json({ message: 'Sale completed and inventory updated successfully!' });
    
    } catch (e) {
        res.status(500).json({ error: e.message })
    }
}

const addItem =  async(req, res) => {
    try {
        const { id, itemId } = req.params
        // find if the item exists
        const sales = await Sales.findByPk(id)
        
        if (!sales) {
            return res.status(404).json({ error: "The sale doesn't exist" })
        }
        if (sales.is_complete) {
            return res.status(409).json({ error: "Cannot add items to a completed order!" })
        }
        const reqObj = (pick(req.body, ["price", "sale_quantity"]))
        const item = await Items.findByPk(itemId)
        if (!item) {
            return res.status(404).json({ error: "The item doesn't exist" })
        }
        const itemExists = await sales.hasItem(item)
        if (itemExists) {
            return res.status(409).json({ error: "The item already exists in the sales" })
        }
        const dbItem = await Inventory.findOne({
            where: {
               item_id:itemId
            }
        });
        if (!dbItem) {
            return res.status(404).json({ error: `Requested item with id: ${itemId} doesn't exits in the inventory!` })
        }
        if(reqObj.sale_quantity > dbItem.in_stock) {
            return res.status(404).json({ error: `Requested item with id: ${itemId} is not in stock.` })
        }

        await sales.addItem(item, {
            through: {
                ...reqObj,
            }
        })

        return res.status(201).json({ message: 'Item added successfully!' })
    } catch (e) {
        res.status(500).json({ error: "internal server error "})
    }
}

//remove items from order line
const removeItem = async (req, res) => {
    try {
        const { id, itemId } = req.params
        // find if the item exists
        const sales = await Sales.findByPk(id)
        if (!sales) {
            return res.status(404).json({ error: "The sale doesn't exist" })
        }
        if (sales.is_complete) {
            return res.status(409).json({ error: "Cannot remove items from a completed sale!" })
        }
        const item = await Items.findByPk(itemId)
        if (!item) {
            return res.status(404).json({ error: "The item doesn't exist" })
        }
        const itemExists = await sales.hasItem(item)
        if (!itemExists) {
            return res.status(404).json({ error: "The item doesn't exist in the sale" })
        }
        await sales.removeItem(item)
        return res.status(200).json({ message: 'Item removed successfully!' })
    } catch (e) {
        res.status(500).json({ error: "internal server error" })
    }
}

const updateItem = async (req, res) => {
    try {
        const { id, itemId } = req.params
        // find if the item exists
        const sales = await Sales.findByPk(id)
        if (!sales) {
            return res.status(404).json({ error: "The sale doesn't exist" })
        }
        if (sales.is_complete) {
            return res.status(409).json({ error: "Cannot update items of a completed sale!" })
        }
        const reqObj = (pick(req.body, ["price", "sale_quantity"]))
        const item = await Items.findByPk(itemId)
        if (!item) {
            return res.status(404).json({ error: "The item doesn't exist" })
        }
        const itemExists = await sales.hasItem(item)
        if (!itemExists) {
            return res.status(404).json({ error: "The item doesn't exists in the sale" })
        }
        const dbItem = await Inventory.findOne({
            where: {
               item_id:itemId
            }
        });
        if(reqObj.sale_quantity > dbItem.in_stock) {
            return res.status(404).json({ error: `Requested item with id: ${itemId} is not in stock.` })
        }

        await sales.addItem(item, {
            through: {
                ...reqObj
            }
        })
        return res.status(200).json({ message: 'Sales item updated successfully!' })
    } catch (e) {
        return res.status(500).json({ error: "internal server error" })
    }
}

const getSalePdf = async (req, res) => {
    try {
        const { id } = req.params
        // find if the sale exists
        const sales = await Sales.findByPk(id, {
            include: [{
                model: Items,
                attributes: ["id", "name", "description", "image_url", "sku_code"],
                include: [
                    {
                        model: Brands,
                        attributes: ["brand_name"]
                    },
                    {
                        model: Categories,
                        attributes: ["category_type"]
                    }
                ]
            }],
        })
        if (!sales) {
            return res.status(404).json({ error: "The sale doesn't exist" })
        }
        // extract required information
        const saleDate = `${sales.sale_date.getFullYear()}/${sales.sale_date.getMonth() + 1}/${sales.sale_date.getDate()}`;

        const doc = new PDFDocument({ size: "A4", margin: 50 })
        res.setHeader('Content-Type', 'application/pdf');
        doc.pipe(res)

        // header info
        doc
            // .image("logo.png", 50, 45, { width: 50 })
            .fillColor("#444444")
            .fontSize(20)
            .text("XYZ Company", 110, 57)
            .fontSize(10)
            .text("XYZ Company", 200, 50, { align: "right" })
            .text("123 lorem address", 200, 65, { align: "right" })
            .text("980-1234567", 200, 80, { align: "right" })
            .text("test@mail.com", 200, 95, { align: "right" })
            .moveDown();

        doc
            .fillColor("#444444")
            .fontSize(20)
            .text("Sale Order", 50, 160);

        // horizontal line
        doc
            .strokeColor("#aaaaaa")
            .lineWidth(1)
            .moveTo(50, 185)
            .lineTo(550, 185)
            .stroke();

        doc
            .fontSize(10)
            .text(`Sale Order Number: ${sales.bill_number}`, 50, 200)
            .font("Helvetica-Bold")
            .font("Helvetica")
            .text(`Order Date: ${saleDate}`, 50, 215)
            .moveDown();

        // horizontal line
        doc
            .strokeColor("#aaaaaa")
            .lineWidth(1)
            .moveTo(50, 240)
            .lineTo(550, 240)
            .stroke();

        // table header
        doc
            .font("Helvetica-Bold")
            .fontSize(10)
            .text('item', 50, 280)
            .text('price', 280, 280, { width: 90, align: "right" })
            .text('quantity', 370, 280, { width: 90, align: "right" })
            .text('total', 0, 280, { align: "right" });

        // hr
        doc
            .strokeColor("#aaaaaa")
            .lineWidth(1)
            .moveTo(50, 300)
            .lineTo(550, 300)
            .stroke();

        doc.font("Helvetica");
        let totalAmount = 0;
        let pos = 0;
        // tr
        sales.Items.forEach((item, i) => {
            const position = 280 + (i + 1) * 30;
            doc
                .fontSize(10)
                .text(item.name, 50, position)
                .text(`Rs.${item.SalesItems.price}`, 280, position, { width: 90, align: "right" })
                .text(item.SalesItems.sale_quantity, 370, position, { width: 90, align: "right" })
                .text(`Rs. ${item.SalesItems.sale_quantity * item.SalesItems.price}`, 0, position, { align: "right" });
            // hr
            doc
                .strokeColor("#aaaaaa")
                .lineWidth(1)
                .moveTo(50, position + 20)
                .lineTo(550, position + 20)
                .stroke();

            totalAmount = totalAmount + (item.SalesItems.sale_quantity * item.SalesItems.price);
            pos++;
        });
        const totalPosition = 280 + (pos + 1) * 30;

        doc
            .font("Helvetica-Bold")
            .fontSize(10)
            .text('', 50, totalPosition)
            .text('', 280, totalPosition, { width: 90, align: "right" })
            .text('total amount:', 370, totalPosition, { width: 90, align: "right" })
            .text(totalAmount, 0, totalPosition, { align: "right" });


        doc
            .fontSize(10)
            .text(
                "Thank you for shopping with us.",
                50,
                580,
                { align: "center", width: 500 }
            );
        doc.end()
        // return res.status(200).json({ message: 'OK!', data: sale })
    } catch (e) {
        console.log(e.message)
        res.status(500).json({ error: e.message })
    }
}


module.exports = {
    index,
    create,
    destroy,
    update,
    completeSale,
    addItem,
    removeItem,
    updateItem,
    getSalePdf
}