'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('sales', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      sale_to: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      sale_address: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      sale_contact: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      bill_number: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      sale_date: {
        type: Sequelize.DATE,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('sales');
  }
};